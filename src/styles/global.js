import { createGlobalStyle } from 'styled-components';

import 'font-awesome/css/font-awesome.css';

createGlobalStyle`
	* {
		margin: 0;
		padding: 0;
		box-sizing: border-box;
		outline: 0; /* tira detalhe ao redor do chrome*/
	}

	body {
		background: #9b65e6;
		text-rendering: optimizeLegibility !important; /* deixa a fonte mais legivel no google chrome */
		-webkit-font-smoothing: antialiased !important;
		font-family: sans-serif;
	}
`;

export default createGlobalStyle;
