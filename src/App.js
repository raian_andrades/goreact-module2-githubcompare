import React, { Fragment } from 'react';

import './styles/global';
// import GlobalStyle from './global-style';

import Main from './pages/Main';

const App = () => (
  <Fragment>
    <Main />
  </Fragment>
);

export default App;
